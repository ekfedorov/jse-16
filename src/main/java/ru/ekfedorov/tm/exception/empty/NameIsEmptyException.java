package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public class NameIsEmptyException extends AbstractException {

    public NameIsEmptyException() throws Exception {
        super("Error! Name is empty...");
    }

}
