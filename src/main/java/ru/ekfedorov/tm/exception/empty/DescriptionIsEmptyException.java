package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public class DescriptionIsEmptyException extends AbstractException {

    public DescriptionIsEmptyException() throws Exception {
        super("Error! Description is empty...");
    }

}
