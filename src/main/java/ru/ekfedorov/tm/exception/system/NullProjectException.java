package ru.ekfedorov.tm.exception.system;

import ru.ekfedorov.tm.exception.AbstractException;

public class NullProjectException extends AbstractException {

    public NullProjectException() throws Exception {
        super("Error! Project is null...");
    }

}
