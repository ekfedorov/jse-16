package ru.ekfedorov.tm.bootstrap;

import ru.ekfedorov.tm.api.controller.ICommandController;
import ru.ekfedorov.tm.api.controller.IProjectController;
import ru.ekfedorov.tm.api.controller.ITaskController;
import ru.ekfedorov.tm.api.repository.ICommandRepository;
import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.api.service.*;
import ru.ekfedorov.tm.constant.ArgumentConst;
import ru.ekfedorov.tm.constant.TerminalConst;
import ru.ekfedorov.tm.controller.CommandController;
import ru.ekfedorov.tm.controller.ProjectController;
import ru.ekfedorov.tm.controller.TaskController;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.incorrect.IncorrectArgumentException;
import ru.ekfedorov.tm.exception.incorrect.IncorrectCommandException;
import ru.ekfedorov.tm.repository.CommandRepository;
import ru.ekfedorov.tm.repository.ProjectRepository;
import ru.ekfedorov.tm.repository.TaskRepository;
import ru.ekfedorov.tm.service.*;
import ru.ekfedorov.tm.util.TerminalUtil;


public class Bootstrap {

    public final ICommandRepository commandRepository = new CommandRepository();

    public final ICommandService commandService = new CommandService(commandRepository);

    public final ICommandController commandController = new CommandController(commandService);

    public final ITaskRepository taskRepository = new TaskRepository();

    public final ITaskService taskService = new TaskService(taskRepository);

    public final IProjectRepository projectRepository = new ProjectRepository();

    public final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    public final ITaskController taskController = new TaskController(taskService, projectTaskService);

    public final IProjectService projectService = new ProjectService(projectRepository);

    public final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    public final ILoggerService loggerService = new LoggerService();

    private void defaultData() throws Exception {
        projectService.add("bbb", "-").setStatus(Status.IN_PROGRESS);
        projectService.add("bab", "-").setStatus(Status.COMPLETE);
        projectService.add("aaa", "-").setStatus(Status.NOT_STARTED);
        projectService.add("ccc", "-").setStatus(Status.COMPLETE);
        taskService.add("bbb", "-").setStatus(Status.IN_PROGRESS);
        taskService.add("aaa", "-").setStatus(Status.NOT_STARTED);
        taskService.add("ccc", "-").setStatus(Status.COMPLETE);
        taskService.add("eee", "-").setStatus(Status.NOT_STARTED);
        taskService.add("ddd", "-").setStatus(Status.IN_PROGRESS);
    }

    public void run(final String... args) throws Exception {
        loggerService.debug("***           TEST          ***");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        defaultData();
        if (parseArgs(args)) commandController.exit();
        while (true) {
            try {
                System.out.println();
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private void parseArg(final String arg) throws Exception {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.ARG_ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default: throw new IncorrectArgumentException(arg);
        }
    }

    private void parseCommand(final String command) throws Exception {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
                break;
            case TerminalConst.CMD_INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.CMD_TASK_LIST:
                taskController.showList();
                break;
            case TerminalConst.CMD_TASK_CREATE:
                taskController.create();
                break;
            case TerminalConst.CMD_TASK_CLEAR:
                taskController.clear();
                break;
            case TerminalConst.CMD_PROJECT_LIST:
                projectController.showList();
                break;
            case TerminalConst.CMD_PROJECT_CREATE:
                projectController.create();
                break;
            case TerminalConst.CMD_PROJECT_CLEAR:
                projectController.clear();
                break;
            case TerminalConst.CMD_TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.CMD_TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case TerminalConst.CMD_TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.CMD_TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.CMD_PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalConst.CMD_PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.CMD_START_PROJECT_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.CMD_START_PROJECT_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.CMD_START_PROJECT_BY_NAME:
                projectController.startProjectByName();
                break;
            case TerminalConst.CMD_FINISH_PROJECT_BY_ID:
                projectController.finishProjectById();
                break;
            case TerminalConst.CMD_FINISH_PROJECT_BY_INDEX:
                projectController.finishProjectByIndex();
                break;
            case TerminalConst.CMD_FINISH_PROJECT_BY_NAME:
                projectController.finishProjectByName();
                break;
            case TerminalConst.CMD_CHANGE_PROJECT_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.CMD_CHANGE_PROJECT_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.CMD_CHANGE_PROJECT_STATUS_BY_NAME:
                projectController.changeProjectStatusByName();
                break;
            case TerminalConst.CMD_START_TASK_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.CMD_START_TASK_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.CMD_START_TASK_BY_NAME:
                taskController.startTaskByName();
                break;
            case TerminalConst.CMD_FINISH_TASK_BY_ID:
                taskController.finishTaskById();
                break;
            case TerminalConst.CMD_FINISH_TASK_BY_INDEX:
                taskController.finishTaskByIndex();
                break;
            case TerminalConst.CMD_FINISH_TASK_BY_NAME:
                taskController.finishTaskByName();
                break;
            case TerminalConst.CMD_CHANGE_TASK_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.CMD_CHANGE_TASK_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.CMD_CHANGE_TASK_STATUS_BY_NAME:
                taskController.changeTaskStatusByName();
                break;
            case TerminalConst.CMD_REMOVE_PROJECT_WITH_TASKS_BY_ID:
                projectController.removeProjectWithTasksById();
                break;
            case TerminalConst.CMD_FIND_AL_LBY_PROJECT_ID:
                taskController.findAllByProjectId();
                break;
            case TerminalConst.CMD_BIND_TASK_BY_PROJECT:
                taskController.bindTaskByProject();
                break;
            case TerminalConst.CMD_UNBIND_TASK_FROM_PROJECT:
                taskController.unbindTaskFromProject();
                break;
            default: throw new IncorrectCommandException(command);
        }
    }

    private void showIncorrectCommand() {
        System.out.println("Error! Command not found...\n");
    }

    private void showIncorrectArgument() {
        System.out.println("Error! Argument not found...");
    }

    private boolean parseArgs(final String... args) throws Exception {
        if (args == null || args.length == 0) return false;
        final String param = args[0];
        parseArg(param);
        return true;
    }

}
