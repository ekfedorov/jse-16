package ru.ekfedorov.tm.api.controller;

public interface IProjectController {

    void showList() throws Exception;

    void create() throws Exception;

    void clear();

    void showProjectByIndex() throws Exception;

    void showProjectByName() throws Exception;

    void showProjectById() throws Exception;

    void removeProjectByIndex() throws Exception;

    void removeProjectByName() throws Exception;

    void removeProjectById() throws Exception;

    void updateProjectByIndex() throws Exception;

    void updateProjectById() throws Exception;

    void startProjectById() throws Exception;

    void startProjectByIndex() throws Exception;

    void startProjectByName() throws Exception;

    void finishProjectById() throws Exception;

    void finishProjectByIndex() throws Exception;

    void finishProjectByName() throws Exception;

    void changeProjectStatusById() throws Exception;

    void changeProjectStatusByIndex() throws Exception;

    void changeProjectStatusByName() throws Exception;

    void removeProjectWithTasksById() throws Exception;

}
