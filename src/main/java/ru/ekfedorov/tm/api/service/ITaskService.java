package ru.ekfedorov.tm.api.service;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator) throws Exception;

    Task add(String name, String description) throws Exception;

    void add(Task task) throws Exception;

    void remove(Task task) throws Exception;

    void clear();

    Task findOneByIndex(Integer index) throws Exception;

    Task findOneById(String id) throws Exception;

    Task findOneByName(String name) throws Exception;

    Task removeOneByIndex(Integer index) throws Exception;

    Task removeOneByName(String name) throws Exception;

    Task removeOneById(String id) throws Exception;

    Task updateTaskByIndex(Integer index, String name, String description) throws Exception;

    Task updateTaskById(String id, String name, String description) throws Exception;

    Task startTaskById(String id) throws Exception;

    Task startTaskByIndex(Integer index) throws Exception;

    Task startTaskByName(String name) throws Exception;

    Task finishTaskById(String id) throws Exception;

    Task finishTaskByIndex(Integer index) throws Exception;

    Task finishTaskByName(String name) throws Exception;

    Task changeTaskStatusById(String id, Status status) throws Exception;

    Task changeTaskStatusByIndex(Integer index, Status status) throws Exception;

    Task changeTaskStatusByName(String name, Status status) throws Exception;

}
