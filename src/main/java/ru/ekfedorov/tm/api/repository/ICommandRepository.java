package ru.ekfedorov.tm.api.repository;

import ru.ekfedorov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
